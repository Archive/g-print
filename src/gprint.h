#ifndef GPRINT_H
#define GPRINT_H

typedef struct {
	char *queue_name;
} PrinterQueue;

typedef GList PrinterQueueList;

char             *gprint_get_printcap_path (void);
PrinterQueueList *gprint_get_queues        (void);

void              gprint_error             (char *msg);
int               gprint_ask               (char *msg);

void              gprint_start_druid       (char *queue_name);

#endif

