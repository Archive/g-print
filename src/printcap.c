/*
 * printcap.c: Handling of the printcap file for loading
 * queue definitions
 *
 * Authors:
 *   Cesar Miquel
 *   Miguel de Icaza
 */
#include <sys/stat.h>
#include <unistd.h>
#include <ctype.h>
#include <gnome.h>
#include "gprint.h"

char *
gprint_get_printcap_path (void)
{
	return gnome_config_get_string_with_default (
		"/Printer/System/printcap_file=/etc/printcap", 
		NULL);
}

static PrinterQueue *
printer_queue_new (char *name)
{
	PrinterQueue *pq;

	pq = g_new (PrinterQueue, 1);
	pq->queue_name = g_strdup (name);

	return pq;
}

static gboolean
line_continues (char *line)
{
	g_strchomp (line);
	if (line [strlen (line)-1] == '\\')
		return TRUE;
	else
		return FALSE;
}

PrinterQueueList *
gprint_get_queues (void)
{
	PrinterQueueList *queues = NULL;
	FILE *fp;
	char *filename;
	char line [4096];
	
	filename = gprint_get_printcap_path ();
	fp = fopen (filename, "r");
	if (!fp){
		char *msg = g_strdup_printf (_("Could not open printcap file: %s"), filename);
		
		gprint_error (msg);
		exit (1);
	}

	while (fgets (line, sizeof (line), fp)){
		PrinterQueue *queue;
		char *queue_name;
		char *p = line;
		
		while (*p && isspace (*p))
			p++;
		if (*p == '#')
			continue;
		if (!*p)
			continue;
		queue_name = p;
		while (*p && *p != ':' && *p != '|' && *p != '\n')
			p++;
		if (*p == ':' || *p == '|'){
			*p = 0;
			p++;
		} 
		queue = printer_queue_new (queue_name);
		queues = g_list_prepend (queues, queue);

		if (line_continues (p)){
			while (fgets (line, sizeof (line), fp)){
				if (!line_continues (line))
					break;
			}
		}
	}

	return queues;
}

