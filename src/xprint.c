/*
 * Gnome Printer
 *
 * Authors:
 *   Cesar Miquel
 *   Miguel de Icaza
 */
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <ctype.h>

#include <config.h>
#include <glib.h>
#include <gnome.h>
#include <libgnome/gnome-fileconvert.h>
#include "gprint.h"

static char *queue;
static char *spooler;
static char *target_mime_type;
static int  no_conversion;
static char *desktop_dir;

static const struct poptOption options [] = {
	{ "queue", 'P', POPT_ARG_STRING, &queue, 0,
	  N_("Specifies the printer queue name"), N_("QUEUE") },
	{ "spooler", 'S', POPT_ARG_STRING, &spooler, 0,
	  N_("Specifies a spooler command"), N_("SPOOLER") },
	{ "target-type", 0, POPT_ARG_STRING, &target_mime_type, 0,
	  N_("Specifies the mime-type of the printer accepted input"), N_("MIME-TYPE") },
	{ "noconv", 0, POPT_ARG_INT, &no_conversion, 0,
	  N_("Specifies that no conversions should be done") },
	{ "create-printers", 0, POPT_ARG_STRING, &desktop_dir, 0, N_("DESKTOP-DIR"),
	  N_("Creates links to this program in the directory specified") }, 
	{ "desktop-dir", 0, POPT_ARG_STRING, &desktop_dir, 0, N_("DESKTOP-DIR"),
	  N_("Creates links to this program in the directory specified") }, 
	{ NULL, 0, 0, NULL, 0 }
};

/**
 * gprint_error
 * @msg: message
 */
void
gprint_error (char *msg)
{
	GtkWidget *w;
	
	w = gnome_message_box_new (msg, 
		GNOME_MESSAGE_BOX_ERROR,
		GNOME_STOCK_BUTTON_OK,
		NULL);
	gnome_dialog_run (GNOME_DIALOG (w));
}

/** 
 * gprint_ask:
 *
 * Asks the user a yes/no question.
 */
int
gprint_ask (char *msg)
{
	GtkWidget *w;
	int retval;
	
	w = gnome_message_box_new (msg, 
		GNOME_MESSAGE_BOX_ERROR,
		GNOME_STOCK_BUTTON_YES,
		GNOME_STOCK_BUTTON_NO,
		NULL);
	retval = gnome_dialog_run (GNOME_DIALOG (w));

	return retval;
}

static void
query_queue (void)
{

}

struct {
	char *spooler_name;
	char *flags;
} spoolers [] = {
	{ "lpr", "-P%s" },
	{ NULL, NULL }
};

static void
init_spooler (void)
{
	int i;
	
	for (i = 0; spoolers [i].spooler_name; i++){
		char *p;
		
		p = gnome_is_program_in_path ("lpr");
		
		if (p){
			spooler = g_strconcat (p, " ", spoolers [i].flags, NULL);
			g_free (p);
			return;
		}
	}

	gprint_error (
		_("I can not locate the printer spooler program.\n"
		  "Try using the --spooler option to specify the command format.\n"));
	exit (1);
}

FILE *
gprint_open_spooler (char *queue)
{
	FILE *f;
	char *command;
	
	if (!spooler)
		init_spooler ();

	command = g_strdup_printf (spooler, queue);
	f = popen (command, "w");
	g_free (command);
	if (!f){
		gprint_error (_("Error starting up the spooler program."));
		
		exit (1);
	}

	return f;
}

static void
copy (FILE *input, FILE *output)
{
	char buffer [8192];
	int n;
	
	while ((n = fread (buffer, 1, sizeof (buffer), input)) > 0)
		fwrite (buffer, 1, n, output);
}

/**
 * gprint_print_direct:
 * @filename: File name to print
 *
 * Sends the contents of the file in @filename to the print spooler
 */
static void
gprint_print_direct (char *filename, char *queue)
{
	FILE *input, *output;

	input = fopen (filename, "r");
	if (!input){
		char *msg;

		msg = g_strdup_printf (_("Can not open file `%s' for printing."), filename);
		
		gprint_error (msg);
		g_free (msg);
		return;
	}
	output = gprint_open_spooler (queue);

	copy (input, output);

	fclose (input);
	pclose (output);
}

static char *
gprint_get_printer_mime_type (void)
{
	char *mime_type;

	mime_type = gnome_config_get_string ("mime-type=application/postscript");

	/*
	 * If the mime type contains %s, it means that the paper
	 * size must be included at the end
	 */
	if (strstr (mime_type, "%s")){
		char *new_mime;
		char *paper = gnome_config_get_string ("paper=letter");
		char *str = g_strconcat ("-", paper, NULL);

		new_mime = g_strdup_printf (mime_type, str);

		g_free (paper);
		g_free (str);
		g_free (mime_type);
		
		return new_mime;
	}
	
	return mime_type;
}

/**
 * gprint_process_file:
 * @output: opened pipe to the spooler program
 * @file: Filename that will be printed
 *
 * Converts the @file to a format suitable for being sent to
 * the print spooler pointed in by @output.
 */
void
gprint_process_file (char *filename, char *queue)
{
	const char *from_mime_type, *to_mime_type;
	FILE *input, *output;
	int fd, do_conversion;

	/* Check for a valid file */
	g_return_if_fail (filename != NULL);

	if (!g_file_exists (filename)){
		char *text;

		text = g_strdup_printf (_("File `%s' could not be opened for reading."), filename);
		gprint_error (text);
		g_free (text);
		
		return;
	}

	if (no_conversion)
		do_conversion = 0;
	else
		do_conversion = gnome_config_get_bool ("DoConversion=false");
	
	if (!do_conversion){
		gprint_print_direct (filename, queue);
		return;
	}
		
	/* Find out mime-type of file */
	from_mime_type = gnome_mime_type_from_magic (filename);
	
	if (!from_mime_type)
		from_mime_type = gnome_mime_type (filename);
	
	if (!from_mime_type){
		char *msg =
			_("Cannot figure out the mime type for %s,\n"
			  "do I print this without conversion?");
		
		if (gprint_ask (msg) == 0){
			gprint_print_direct (filename, queue);
			return;
		}
		return;
	}
	
	if (target_mime_type)
		to_mime_type = target_mime_type;
	else
		to_mime_type = gprint_get_printer_mime_type ();
	
	if (!to_mime_type){
		gprint_print_direct (filename, queue);
		return;
	}
	
	if (strcmp (to_mime_type, from_mime_type) == 0){
		gprint_print_direct (filename, queue);
		return;
	}
	
	/* Process file with gnome_fileconvert */
	fd = -1;
	fd = gnome_file_convert (filename, from_mime_type, to_mime_type);
	
	if (fd < 0){
		char *msg =
			_("Conversion of file %s from %s to %s failed.\n"
			  "Do you want to send the raw file to the printer?");
		
		if (gprint_ask (msg) == 0)
			gprint_print_direct (filename, queue);
		return;
	}
	
	output = gprint_open_spooler (queue);
	input = fdopen (fd, "r");
	
	copy (input, output);
	
	fclose (input);
	pclose (output);
	return;
}

static void
monitor_printer (void)
{
	exit (0);
}

static void
make_devices (char *name)
{
	PrinterQueueList *queues, *l;
	char dir [8192];
	int printers;
	
	queues = gprint_get_queues ();

	if (chdir (desktop_dir) == -1)
		return;

	getcwd (dir, sizeof (dir));

	printers = g_list_length (queues);

	if (printers > 3){
		char *msg = g_strdup_printf (
			_("There are %d printers defined on this system.\n"
			  "Do you want to create shortcuts to every one?"), printers);

			if (gprint_ask (msg) == 1){
				queues->next->next->next = NULL;
			}
	}
	for (l = queues; l; l = l->next){
		PrinterQueue *queue = l->data;
		char *str;
		struct stat buf;
		
		str = g_strdup_printf ("printer.%s", queue->queue_name);

		if (lstat (str, &buf) == 0){
			if (S_ISLNK (buf.st_mode))
				unlink (str);
		}
		
		if (symlink (name, str) == 0){
			char *cmd, *cap, *icon, *full;

			full = g_concat_dir_and_file (dir, str);

			printf ("Setting for %s\n", full);
			cmd = g_strconcat (full, " --queue ", queue->queue_name, " %q", NULL);
			gnome_metadata_set (full, "drop-action", strlen (cmd)+1, cmd);
			g_free (cmd);

			cap = g_strdup_printf (_("Printer %s"), queue->queue_name);
			gnome_metadata_set (full, "icon-caption", strlen (cap)+1, cap);
			g_free (cap);

			icon = gnome_unconditional_pixmap_file ("mc/i-printer.png");
			printf ("%s\n", icon);
			gnome_metadata_set (full, "icon-filename", strlen (icon)+1, icon);
			g_free (icon);

			g_free (full);
		}
		g_free (str);
	}
}

int
main (int argc, char *argv [])
{
	poptContext ctx;
	char *queue_prefix;
	char *file, *original_queue;

	bindtextdomain(PACKAGE, GNOMELOCALEDIR);
	textdomain(PACKAGE);
	
	gnome_init_with_popt_table ("GnomePrinter", VERSION, argc, argv, options, 0, &ctx);

	if (desktop_dir){
		make_devices (argv [0]);
		exit (0);
	}
	
	original_queue = queue;
	
	if (!queue){
		queue = gnome_config_get_string ("/Printer/Printer/DefaultQueue=");
		if (!queue || (queue && *queue == 0))
			query_queue ();
	}

	queue_prefix = g_strconcat ("/Printer/Queue:", queue, "/", NULL);
	gnome_config_push_prefix (queue_prefix);
	g_free (queue_prefix);

	if (!gnome_config_get_bool ("configured=false")){
		gprint_start_druid (queue);
		gtk_main ();
	}
	
	file = poptGetArg (ctx);
	if (file){
		do {
			gprint_process_file (file, queue);
		} while ((file = poptGetArg (ctx)) != NULL);

		return 0;
	} else {
		gprint_start_druid (queue);
		gtk_main ();
	}

	return 0;
}
