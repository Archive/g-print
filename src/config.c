/*
 * config.c: The Druid for configuring the printer
 *
 * Authors:
 *   Cesar Miquel
 *   Miguel de Icaza
 */
#include <sys/stat.h>
#include <unistd.h>
#include "config.h"
#include <gnome.h>
#include <glib.h>
#include <ctype.h>
#include "gprint.h"

PrinterQueueList *gprint_queues;

typedef struct {
	GtkWidget *toplevel;
	GnomeDruid *druid;
	
	GtkWidget *r_conv;
	GtkWidget *r_noconv;

	GtkWidget *types;	/* Option Menu with a list of printers */
	GtkWidget *paper_select;
	char *selected_queue_name;

	char *selected_mime;
	char *paper;
} PrinterDruid;

static void
select_queue (PrinterDruid *druid, char *queue_name)
{
	druid->selected_queue_name = queue_name;
}

static void
choose_queue (GtkObject *item, char *queue_name)
{
	PrinterDruid *druid = gtk_object_get_user_data (item);
	
	select_queue (druid, queue_name);
}

static GtkWidget *
create_queue_chooser_page (PrinterDruid *druid)
{
	PrinterQueueList *l;
	GtkWidget *vbox1;
	GtkWidget *label2;
	GtkWidget *hbox1;
	GtkWidget *label1;
	GtkWidget *alignment1;
	GtkWidget *optionmenu2;
	GtkWidget *optionmenu2_menu;

	vbox1 = gtk_vbox_new (FALSE, 10);

	label2 = gtk_label_new (
		_("Choose the queue that you want to configure from the\n"
		  "following list."));
	
	gtk_widget_show (label2);
	gtk_box_pack_start (GTK_BOX (vbox1), label2, TRUE, TRUE, 0);
	gtk_widget_set_usize (label2, 200, 22);
	gtk_label_set_justify (GTK_LABEL (label2), GTK_JUSTIFY_LEFT);
	gtk_misc_set_alignment (GTK_MISC (label2), 0.2, 0.2);

	hbox1 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox1);
	gtk_box_pack_start (GTK_BOX (vbox1), hbox1, FALSE, TRUE, 5);
	gtk_widget_set_usize (hbox1, -1, 43);

	label1 = gtk_label_new (_("Printer queue:"));
	gtk_widget_show (label1);
	gtk_box_pack_start (GTK_BOX (hbox1), label1, FALSE, TRUE, 3);
	gtk_widget_set_usize (label1, 110, -1);
	gtk_label_set_justify (GTK_LABEL (label1), GTK_JUSTIFY_RIGHT);
	gtk_misc_set_alignment (GTK_MISC (label1), 1, 0.5);

	alignment1 = gtk_alignment_new (0.5, 0.5, 1, 1);
	gtk_widget_show (alignment1);
	gtk_box_pack_start (GTK_BOX (hbox1), alignment1, FALSE, TRUE, 0);
	gtk_widget_set_usize (alignment1, 97, 20);

	optionmenu2 = gtk_option_menu_new ();
	gtk_widget_show (optionmenu2);
	gtk_container_add (GTK_CONTAINER (alignment1), optionmenu2);
	gtk_widget_set_usize (optionmenu2, -1, 20);
	gtk_container_border_width (GTK_CONTAINER (optionmenu2), 1);
	optionmenu2_menu = gtk_menu_new ();
	gtk_option_menu_set_menu (GTK_OPTION_MENU (optionmenu2), optionmenu2_menu);

	/* Parse printcap and add available queues to menu */
	for (l = gprint_queues; l; l = l->next){
		PrinterQueue *queue = l->data;
		GtkWidget *item;

		item = gtk_menu_item_new_with_label (queue->queue_name);
		gtk_object_set_user_data (GTK_OBJECT (item), druid);
		gtk_widget_show (item);
		gtk_menu_append (GTK_MENU (optionmenu2_menu), item);
		gtk_signal_connect (
			GTK_OBJECT (item), "activate",
			GTK_SIGNAL_FUNC (choose_queue), queue->queue_name);
	}

	gtk_widget_show_all (vbox1);
	gtk_widget_hide (vbox1);
	
	return vbox1;

}

static struct {
	char *full_name;
	char *mime_format;
} known_printers [] = {
	{ N_("No conversion (pass this verbatim to printer)"), NULL },
	{ N_("PostScript printer"),      "application/postscript" },
	{ N_("HP LaserJet 3 at 300dpi"), "x-printer/ljet3-300-300%s" }, 
	{ N_("HP LaserJet 4 at 300dpi"), "x-printer/ljet4-300-300%s" }, 
	{ N_("HP LaserJet 4 at 600dpi"), "x-printer/ljet4-600-600%s" },
	{ N_("HP LaserJet"),             "x-printer/laserjet-x-x%s" },
	{ N_("HP LaserJet Plus"),  	 "x-printer/ljetplus-x-x%s" },
	{ N_("HP LaserJet 2P"),    	 "x-printer/ljet2p-x-x%s" },
	{ N_("HP LaserJet 3D"),    	 "x-printer/ljet3d-x-x%s" },
	{ N_("HP DeskJet"),        	 "x-printer/desktjet-x-x%s" },
	{ N_("HP DeskJet 500"),    	 "x-printer/djet500-x-x%s" },
	{ N_("HP DeskJet 500c"),   	 "x-printer/djet500c-x-x%s" },
	{ N_("HP Color DeskJet"),  	 "x-printer/cdeskjet-x-x%s" },
	{ N_("HP Color DeskJet 500"),    "x-printer/cdj500-x-x%s" },
	{ N_("HP Color DeskJet 550"),    "x-printer/cdj550-x-x%s" },
	{ N_("HP Color DeskJet"),   	 "x-printer/cdeskjet-x-x%s" },
	{ N_("PJXL"),               	 "x-printer/pjxl-x-x%s" },
	{ N_("PJXL at 300dpi"),     	 "x-printer/pjxl300-x-x%s" },
	{ N_("Bubble Jet 10e"),     	 "x-printer/bj10e-x-x%s" },
	{ N_("Bubble Jet 200"),     	 "x-printer/bj200-x-x%s" },
	{ N_("Bubble Jet Color 600"),    "x-printer/bjc-600-x-x%s" },
	{ N_("FAX, g3 format"),      	 "x-printer/faxg3-x-x%s" },
	{ N_("FAX, g32d format"),    	 "x-printer/faxg32d-x-x%s" },
	{ N_("FAX, g4 format"),      	 "x-printer/faxg4-x-x%s" },
	{ N_("Epson color"),             "x-printer/epsonc-x-x%s" },
	{ N_("IBM ProPrinter"),          "x-printer/ibmpro-x-x%s" },
	{ NULL, NULL }
};

static void
set_printer_mime (GtkWidget *widget, PrinterDruid *druid)
{
	char *string = gtk_object_get_user_data (GTK_OBJECT (widget));

	if (druid->selected_mime)
		g_free (druid->selected_mime);
	if (string)
		druid->selected_mime = g_strdup (string);
	else
		druid->selected_mime = NULL;
}

static GtkWidget *
create_printer_types (PrinterDruid *druid)
{
	GtkWidget *option_menu;
	GtkWidget *menu;
	int i, select;
	
	option_menu = gtk_option_menu_new ();
	gtk_widget_show (option_menu);
	menu = gtk_menu_new ();
	gtk_widget_show (menu);
	gtk_option_menu_set_menu (GTK_OPTION_MENU (option_menu), menu);

	select = 0;
	for (i = 0; known_printers [i].full_name != NULL; i++){
		GtkWidget *item;
		char *label, *mime_format;

		label = known_printers [i].full_name;
		mime_format = known_printers [i].mime_format;

		if (druid->selected_mime == mime_format)
			select = i;
		else if (druid->selected_mime && mime_format)
			if (strcmp (druid->selected_mime, mime_format) == 0)
				select = i;
		
		item = gtk_menu_item_new_with_label (_(label));
		gtk_widget_show (item);
		gtk_menu_append (GTK_MENU (menu), item);
		gtk_object_set_user_data (GTK_OBJECT (item), mime_format);
		gtk_signal_connect (GTK_OBJECT (item), "activate",
				    set_printer_mime, druid);
	}

	gtk_option_menu_set_history (GTK_OPTION_MENU (option_menu), select);
	return option_menu;
}

static struct {
	char *full_name;
	char *name;
} known_papers [] = {
	{ N_("Letter"), "letter" },
	{ N_("Legal"),  "legal" },
	{ N_("A4"),     "a4" },
	{ NULL, NULL }
};

static void
set_paper_size (GtkWidget *widget, PrinterDruid *druid)
{
	char *string = gtk_object_get_user_data (GTK_OBJECT (widget));

	g_free (druid->paper);
	druid->paper = g_strdup (string);
}

static GtkWidget *
create_paper_types (PrinterDruid *druid)
{
	GtkWidget *option_menu;
	GtkWidget *menu;
	int i, select;
	
	option_menu = gtk_option_menu_new ();
	gtk_widget_show (option_menu);
	menu = gtk_menu_new ();
	gtk_widget_show (menu);
	gtk_option_menu_set_menu (GTK_OPTION_MENU (option_menu), menu);

	select = 0;
	for (i = 0; known_papers [i].full_name != NULL; i++){
		GtkWidget *item;
		char *label, *format;

		label = known_papers [i].full_name;
		format = known_papers [i].name;

		if (strcmp (format, druid->paper) == 0)
			select = i;
		
		item = gtk_menu_item_new_with_label (_(label));
		gtk_widget_show (item);
		gtk_menu_append (GTK_MENU (menu), item);
		gtk_object_set_user_data (GTK_OBJECT (item), format);
		gtk_signal_connect (GTK_OBJECT (item), "activate",
				    set_paper_size, druid);
	}

	gtk_option_menu_set_history (GTK_OPTION_MENU (option_menu), select);
	return option_menu;
}

static GtkWidget *
create_mime_type_page (PrinterDruid *druid)
{
	GtkWidget *page, *l;
	
	page = gtk_table_new (0, 0, 0);

	l = gtk_label_new (
		_("Select whether this queue will do automatic conversions\n"
		  "of the data passed to one of the following designated types\n"));
	gtk_label_set_justify (GTK_LABEL (l), GTK_JUSTIFY_LEFT);
		
	druid->r_noconv = gtk_radio_button_new_with_label (
		NULL, _("No format conversion"));
	druid->r_conv = gtk_radio_button_new_with_label_from_widget (
		GTK_RADIO_BUTTON (druid->r_noconv),
		_("Convert to this type:"));

	if (druid->selected_mime == NULL)
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (druid->r_noconv), TRUE);
	else
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (druid->r_conv), TRUE);

	gtk_table_attach (
		GTK_TABLE (page), l,
		0, 5, 0, 1, 0, 0, 0, 0);

	/*
	 * Printer type
	 */
	druid->types = create_printer_types (druid);
	gtk_table_attach (
		GTK_TABLE (page), druid->r_noconv,
		1, 2, 2, 3, GTK_FILL, 0, 0, 0);
	gtk_table_attach (
		GTK_TABLE (page), druid->r_conv,
		1, 2, 3, 4, GTK_FILL, 0, 0, 0);
	gtk_table_attach (
		GTK_TABLE (page), druid->types,
		2, 6, 3, 4, GTK_FILL | GTK_EXPAND, GTK_FILL, 0, 0);

	return page;
}

static GtkWidget *
create_paper_page (PrinterDruid *druid)
{
	GtkWidget *page, *l;
	
	page = gtk_table_new (0, 0, 0);

	l = gtk_label_new (
		_("Select whether this queue will do automatic conversions\n"
		  "of the data passed to one of the following designated types\n"));
	gtk_label_set_justify (GTK_LABEL (l), GTK_JUSTIFY_LEFT);
	gtk_table_attach (
		GTK_TABLE (page), l,
		0, 5, 0, 1, 0, 0, 0, 0);

	/*
	 * Paper type
	 */
	l = gtk_label_new (_("Paper type:"));
	gtk_misc_set_alignment (GTK_MISC (l), 0.0, 0.5);
	druid->paper_select = create_paper_types (druid);
	gtk_table_attach (
		GTK_TABLE (page), l,
		1, 2, 4, 5, GTK_FILL, 0, 0, 0);
	gtk_table_attach (
		GTK_TABLE (page), druid->paper_select,
		2, 6, 4, 5, GTK_FILL | GTK_EXPAND, GTK_FILL, GNOME_PAD, 0);

	gtk_widget_show_all (page);
	gtk_widget_hide (page);
	return page;
}

static GtkWidget *
create_finish_page (PrinterDruid *druid)
{
	GtkWidget *page;

	page = gtk_label_new (_("The printer queue has now been configured"));

	return page;
}

static void
printer_druid_destroy (PrinterDruid *druid)
{
	gtk_widget_destroy (druid->toplevel);
	g_free (druid);
}

static struct {
	char *label;
	GtkWidget *(*create_page)(PrinterDruid *druid);
} printer_config_pages [] = {
	{ N_("Choose Printer Queue"), create_queue_chooser_page },
	{ N_("Select Printer Type"),  create_mime_type_page },
	{ N_("Select Paper Size"),    create_paper_page },
	{ N_("Finish"), create_finish_page },
	{ NULL, NULL }
};

static void
gprint_druid_cancel (GnomeDruid *w, PrinterDruid *druid)
{
	char *msg = _("Printer configuration cancelled.\n"
		      "Do you want to configure another printer queue?");

	printer_druid_destroy (druid);

	if (gprint_ask (msg) == 0)
		gprint_start_druid (NULL);
	else
		gtk_main_quit ();
}

static void
gprint_druid_finish (GnomeDruid *w, PrinterDruid *druid)
{
	gnome_config_set_bool   ("DoConversion", druid->selected_mime != NULL);
	gnome_config_set_string ("paper", druid->paper);
	gnome_config_set_string ("mime-type", druid->selected_mime);
	gnome_config_set_bool   ("configured", TRUE);
	gnome_config_sync ();
	printer_druid_destroy (druid);
	gtk_main_quit ();
}

static GtkWidget *
druid_end_page (void)
{
	GtkWidget *end_druid, *l;

	end_druid = gnome_druid_page_finish_new ();

	l = gtk_label_new (
		_("Congratulations!  You have successfully configured your printer"));;
	gtk_container_add (GTK_CONTAINER (end_druid), l);
	gtk_widget_show_all (end_druid);

	return end_druid;
}

void 
gprint_start_druid (char *queue_name)
{
	PrinterDruid *druid;
	GtkWidget *graphic, *end_page;
	gchar *pix;
	int start_page, i;

	if (!gprint_queues)
	        gprint_queues = gprint_get_queues ();
	
	if (!gprint_queues){
		gprint_error (_("I did not find any printer queues configured on this system"));
		return;
	}

	druid = g_new0 (PrinterDruid, 1);

	druid->toplevel = gnome_dialog_new (
		_("Printer Configuration"), NULL);
	gtk_window_set_policy (GTK_WINDOW (druid->toplevel), FALSE, TRUE, FALSE);

	pix = gnome_pixmap_file ("gnome-logo-large.png"); 
	graphic = gnome_pixmap_new_from_file_at_size (pix, 200, 200); 
	g_free(pix); 

	druid->druid = GNOME_DRUID (gnome_druid_new ());
	gtk_container_add (GTK_CONTAINER (druid->toplevel), GTK_WIDGET (druid->druid));
	
	if (gnome_config_get_bool ("DoConversion=false")){
		druid->selected_mime = gnome_config_get_string ("mime-type");
	}  else
		druid->selected_mime = NULL;
		
 	druid->paper = gnome_config_get_string ("paper=letter");
				
	if (queue_name != NULL){
		start_page = 1;
		druid->selected_queue_name = queue_name;
	} else {
		start_page = 0;
	}

	/* Create the pages */
	for (i = start_page; printer_config_pages [i].label; i++){
		GtkWidget *page, *content;

		if (i == start_page)
			page = gnome_druid_page_start_new ();
		else
			page = gnome_druid_page_standard_new ();
		
		content = (*printer_config_pages [i].create_page)(druid);
		gtk_container_add (GTK_CONTAINER (GNOME_DRUID_PAGE_STANDARD (page)->vbox),
				   content);

		gtk_widget_show_all (page);
		
		gnome_druid_append_page (druid->druid, GNOME_DRUID_PAGE (page));
	}

	end_page = druid_end_page ();
	gnome_druid_append_page (druid->druid, GNOME_DRUID_PAGE (end_page));
	
	/* Setup the druid */
	gtk_signal_connect (GTK_OBJECT (end_page), "finish",
			    GTK_SIGNAL_FUNC (gprint_druid_finish),
			    druid);

	gtk_signal_connect (GTK_OBJECT (druid->druid), "cancel",
			    GTK_SIGNAL_FUNC (gprint_druid_cancel),
			    druid);

	gtk_widget_queue_resize (druid->toplevel);
	gtk_widget_show_all (druid->toplevel);
}
