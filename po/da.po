# Danish translation of g-print (gmc plugin)
# Copyright (C) 1999 Free Software Foundation, Inc.
# Kenneth Christiansen <kenneth@ripen.dk>, 1999.
#
msgid ""
msgstr ""
"Project-Id-Version: g-print\n"
"POT-Creation-Date: 2000-02-13 22:33+0100\n"
"PO-Revision-Date: 2000-02-13 22:49+01:00\n"
"Last-Translator: Birger Langkjer <birger.langkjer@image.dk>\n"
"Language-Team: Danish <sslug-locale@sslug.dk>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ISO-8859-1\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/config.c:62
msgid ""
"Choose the queue that you want to configure from the\n"
"following list."
msgstr ""
"V�lg den k� som du vil konfigurere fra den\n"
"f�lgende liste."

#: src/config.c:76
msgid "Printer queue:"
msgstr "Printerk�:"

#: src/config.c:121
msgid "No conversion (pass this verbatim to printer)"
msgstr "Ingen konversion (send u�ndret til printer)"

#: src/config.c:122
msgid "PostScript printer"
msgstr "PostScript printer"

#: src/config.c:123
msgid "HP LaserJet 3 at 300dpi"
msgstr "HP LaserJet 3 ved 300dpi"

#: src/config.c:124
msgid "HP LaserJet 4 at 300dpi"
msgstr "HP LaserJet 4 ved 300dpi"

#: src/config.c:125
msgid "HP LaserJet 4 at 600dpi"
msgstr "HP LaserJet 4 ved 600dpi"

#: src/config.c:126
msgid "HP LaserJet"
msgstr "HP LaserJet"

#: src/config.c:127
msgid "HP LaserJet Plus"
msgstr "HP LaserJet Plus"

#: src/config.c:128
msgid "HP LaserJet 2P"
msgstr "HP LaserJet 2P"

#: src/config.c:129
msgid "HP LaserJet 3D"
msgstr "HP LaserJet 3D"

#: src/config.c:130
msgid "HP DeskJet"
msgstr "HP DeskJet"

#: src/config.c:131
msgid "HP DeskJet 500"
msgstr "HP DeskJet 500"

#: src/config.c:132
msgid "HP DeskJet 500c"
msgstr "HP DeskJet 500c"

#: src/config.c:133 src/config.c:136
msgid "HP Color DeskJet"
msgstr "HP Color DeskJet"

#: src/config.c:134
msgid "HP Color DeskJet 500"
msgstr "HP Color DeskJet 500"

#: src/config.c:135
msgid "HP Color DeskJet 550"
msgstr "HP Color DeskJet 550"

#: src/config.c:137
msgid "PJXL"
msgstr "PJXL"

#: src/config.c:138
msgid "PJXL at 300dpi"
msgstr "PJXL ved 300dpi"

#: src/config.c:139
msgid "Bubble Jet 10e"
msgstr "Bubble Jet 10e"

#: src/config.c:140
msgid "Bubble Jet 200"
msgstr "Bubble Jet 200"

#: src/config.c:141
msgid "Bubble Jet Color 600"
msgstr "Bubble Jet Color 600"

#: src/config.c:142
msgid "FAX, g3 format"
msgstr "FAX, g3 format"

#: src/config.c:143
msgid "FAX, g32d format"
msgstr "FAX, g32d format"

#: src/config.c:144
msgid "FAX, g4 format"
msgstr "FAX, g4 format"

#: src/config.c:145
msgid "Epson color"
msgstr "Epson color"

#: src/config.c:146
msgid "IBM ProPrinter"
msgstr "IBM ProPrinter"

#: src/config.c:206
msgid "Letter"
msgstr "Brev"

#: src/config.c:207
msgid "Legal"
msgstr "Legal"

#: src/config.c:208
msgid "A4"
msgstr "A4"

#: src/config.c:265 src/config.c:309
msgid ""
"Select whether this queue will do automatic conversions\n"
"of the data passed to one of the following designated types\n"
msgstr ""
"V�lg hvorvidt denne printerk� automatisk skal konvertere\n"
"tilsendt data til �n af de f�lgende angivede typer\n"

#: src/config.c:270
msgid "No format conversion"
msgstr "Ingen formatkonversion"

#: src/config.c:273
msgid "Convert to this type:"
msgstr "Konvert�r til denne type:"

#.
#. * Paper type
#.
#: src/config.c:319
msgid "Paper type:"
msgstr "Papirtype:"

#: src/config.c:339
msgid "The printer queue has now been configured"
msgstr "Printerk�en er nu blevet konfigureret"

#: src/config.c:355
msgid "Choose Printer Queue"
msgstr "V�lg printerk�"

#: src/config.c:356
msgid "Select Printer Type"
msgstr "V�lg printertype"

#: src/config.c:357
msgid "Select Paper Size"
msgstr "V�lg papirst�rrelse"

#: src/config.c:358
msgid "Finish"
msgstr "F�rdig"

#: src/config.c:365
msgid ""
"Printer configuration cancelled.\n"
"Do you want to configure another printer queue?"
msgstr ""
"Printerkonfiguration blev annulleret.\n"
"Vil du konfigurere en anden printerk�?"

#: src/config.c:396
msgid "Congratulations!  You have successfully configured your printer"
msgstr "Tillykke!  Du har med held konfigureret din printer"

#: src/config.c:415
msgid "I did not find any printer queues configured on this system"
msgstr "Jeg fandt ikke nogle printerk�er konfigureret p� dette system"

#: src/config.c:422
msgid "Printer Configuration"
msgstr "Printerkonfiguration"

#: src/printcap.c:55
#, c-format
msgid "Could not open printcap file: %s"
msgstr "Kunne ikke �bne printcap-fil: %s"

#: src/xprint.c:28
msgid "Specifies the printer queue name"
msgstr "Angiv navnet p� printerk�en"

#: src/xprint.c:28
msgid "QUEUE"
msgstr "QUEUE"

#: src/xprint.c:30
msgid "Specifies a spooler command"
msgstr "Angiver en spole-kommando"

#: src/xprint.c:30
msgid "SPOOLER"
msgstr "SPOOLER"

#: src/xprint.c:32
msgid "Specifies the mime-type of the printer accepted input"
msgstr "Angiver mine-typen for printerens accepterede inddata"

#: src/xprint.c:32
msgid "MIME-TYPE"
msgstr "MIME-TYPE"

#: src/xprint.c:34
msgid "Specifies that no conversions should be done"
msgstr "Angiver at ingen konversion skal foretages"

#: src/xprint.c:35 src/xprint.c:37
msgid "DESKTOP-DIR"
msgstr "DESKTOP-DIR"

#: src/xprint.c:36 src/xprint.c:38
msgid "Creates links to this program in the directory specified"
msgstr "Opretter l�nker til dette program i den angivede mappe"

#: src/xprint.c:111
msgid ""
"I can not locate the printer spooler program.\n"
"Try using the --spooler option to specify the command format.\n"
msgstr ""
"Jeg kan ikke finde printerspoleprogrammet.\n"
"Pr�v at bruge '--spooler' flaget for at angive kommandoformattet.\n"

#: src/xprint.c:129
msgid "Error starting up the spooler program."
msgstr "Fejl ved opstart af spole-program."

#: src/xprint.c:162
#, c-format
msgid "Can not open file `%s' for printing."
msgstr "Kan ikke �bne filen '%s' for udskrivning."

#: src/xprint.c:225
#, c-format
msgid "File `%s' could not be opened for reading."
msgstr "Filen '%s' kunne ikke �bnes for l�sning."

#: src/xprint.c:250
#, c-format
msgid ""
"Cannot figure out the mime type for %s,\n"
"do I print this without conversion?"
msgstr ""
"Kan ikke finde mimetypen for %s,\n"
"skal jeg printe uden konversion?"

#: src/xprint.c:281
#, c-format
msgid ""
"Conversion of file %s from %s to %s failed.\n"
"Do you want to send the raw file to the printer?"
msgstr ""
"Konversion af filen %s fra %s til %s fejlede.\n"
"Vil du sende den r� fil til printeren?"

#: src/xprint.c:323
#, c-format
msgid ""
"There are %d printers defined on this system.\n"
"Do you want to create shortcuts to every one?"
msgstr ""
"Der er %d printere angivet for dette system.\n"
"Vil du oprette genveje til dem alle?"

#: src/xprint.c:352
#, c-format
msgid "Printer %s"
msgstr "Printer %s"
